package com.fusion.less.controller.base;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fusion.less.entity.base.vo.TeamVO;
import com.fusion.less.service.base.TeamService;
import com.fusion.less.utils.json.Msg;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author cola
 * @param <TeamVO>
 *
 */
@Api(value = "组别操作接口", tags = "组别操作接口")
@RestController
@RequestMapping(value = "/team")
public class TeamController {
	
	@Autowired
	private TeamService teamService;
	
	@ApiOperation(value = "获取全部组别信息")
	@GetMapping("/list")
    public Msg<?> list() {
        return teamService.list();
    }
	
	@ApiOperation(value = "获取组别信息")
	@GetMapping("/{id}")
    public Msg<?> get(@PathVariable int id) {
        return teamService.get(id);
    }
	
	@ApiOperation(value="修改组别")
    @PutMapping("/{id}")
    public Msg<?> update(@PathVariable int id, @RequestBody TeamVO teamVO) {
		teamVO.setId(id);
		return teamService.update(teamVO);
    }
	
	@ApiOperation(value="添加组别")
	@PostMapping
    public Msg<?> add(@RequestBody TeamVO teamVO) {
    	return teamService.add(teamVO);
    }
	
	@ApiOperation(value="删除组别")
	@DeleteMapping("/{id}")
    public Msg<?> delete(@PathVariable int id) {
    	return teamService.delete(id);
    }
}

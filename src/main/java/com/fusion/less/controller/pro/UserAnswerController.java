package com.fusion.less.controller.pro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fusion.less.entity.pro.vo.UserAnswerVO;
import com.fusion.less.service.pro.UserAnswerService;
import com.fusion.less.utils.json.Msg;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/** 
* @author cola 
* @version 创建时间：2019年9月8日 下午5:12:59 
*   说明
*/
@Api(value = "用户提交答案操作接口", tags = "用户提交答案操作接口")
@RestController
@RequestMapping(value = "/userAnswer")
public class UserAnswerController {
	
	@Autowired
	private UserAnswerService userAnswerService;
	
	@ApiOperation(value = "增加提交答案")
	@PostMapping
	public Msg<?> add(@RequestBody List<UserAnswerVO> userAnswerVOList) {
		return userAnswerService.add(userAnswerVOList);
	}
	
}

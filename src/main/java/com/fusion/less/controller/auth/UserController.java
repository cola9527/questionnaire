package com.fusion.less.controller.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fusion.less.entity.auth.vo.UserVO;
import com.fusion.less.service.auth.UserService;
import com.fusion.less.utils.json.Msg;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/** 
* @author cola 
* @version 创建时间：2019年9月8日 下午2:06:46 
*   说明
*/

@Api(value = "用户操作接口",tags = "用户操作接口")
@RestController
@RequestMapping(value = "/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@ApiOperation(value = "获取用户信息")
	@GetMapping("/{id}")
    public Msg<?> get(@PathVariable int id) {
        return userService.get(id);
    }
	
	@ApiOperation(value = "修改用户信息(修改用户的teamId和parentId)")
	@PutMapping("/{id}")
	public Msg<?> update(@PathVariable int id, @RequestBody UserVO userVO) {
		userVO.setId(id);
		return userService.update(userVO);
	}
}


package com.fusion.less;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;


@MapperScan("com.fusion.less.mapper")
@SpringBootApplication
@EnableAsync
public class LessApplication {

	public static void main(String[] args) {
		SpringApplication.run(LessApplication.class, args);
	}

}


package com.fusion.less.service.base;

import com.fusion.less.entity.base.vo.TeamVO;
import com.fusion.less.utils.json.Msg;

public interface TeamService {
	
	/**
	 *删除组别
	 * @param id
	 * @return
	 */
	Msg<?> list();
	
	/**
	 * 查询组别
	 * @param id
	 * @return
	 */
	Msg<?> get(int id);

	/**
	 * 修改组别
	 * @param TeamVO team
	 * @return
	 */
	Msg<?> update(TeamVO teamVO);

	/**
	 * 增加组别
	 * @param TeamVO team
	 * @return
	 */
	Msg<?> add(TeamVO teamVO);
	
	/**
	 *删除组别
	 * @param id
	 * @return
	 */
	Msg<?> delete(int id);

	
}

package com.fusion.less.service.base.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fusion.less.entity.base.vo.TeamVO;
import com.fusion.less.mapper.base.TeamMapper;
import com.fusion.less.service.base.TeamService;
import com.fusion.less.utils.json.Msg;
@Service
public class TeamServiceImpl implements TeamService {
	
	@Autowired
	private TeamMapper teamMapper;
	
	@Override
	public Msg<?> list() {
		ArrayList<TeamVO> list = teamMapper.list();
		return new Msg<>(list);
	};

	@Override
	public Msg<?> get(int id) {
		TeamVO teamVO = teamMapper.get(id);
		return new Msg<>(teamVO);
	}

	@Override
	public Msg<?> update(TeamVO teamVO) {
		teamMapper.update(teamVO);
		return new Msg<>();
	}

	@Override
	public Msg<?> add(TeamVO teamVO) {
		teamMapper.add(teamVO);
		return new Msg<>();
	}

	@Override
	public Msg<?> delete(int id) {
		teamMapper.delete(id);
		return new Msg<>();
	}

}

package com.fusion.less.service.auth;

import com.fusion.less.entity.auth.vo.UserVO;
import com.fusion.less.utils.json.Msg;

/** 
* @author cola 
* @version 创建时间：2019年9月8日 下午2:13:00 
*   说明
*/

public interface UserService {
	
	/**
	 * 查询用户
	 * @param id
	 * @return
	 */
	Msg<?> get(int id);
	
	/**
	 * 根据组别查询BD
	 * @param id
	 * @return
	 */
	Msg<?> get();
	
	/**
	 *修改用户组别、BD信息
	 * @param id
	 * @return
	 */
	Msg<?> update(UserVO userVO);
}

package com.fusion.less.service.auth.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fusion.less.entity.auth.vo.UserVO;
import com.fusion.less.mapper.auth.UserMapper;
import com.fusion.less.service.auth.UserService;
import com.fusion.less.utils.json.Msg;

/** 
* @author cola 
* @version 创建时间：2019年9月8日 下午2:18:14 
*   说明
*/
@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserMapper userMapper;

	@Override
	public Msg<?> get(int id) {
		// TODO Auto-generated method stub
		UserVO userVO = userMapper.get(id);
		return new Msg<>(userVO);
	}

	@Override
	public Msg<?> get() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Msg<?> update(UserVO userVO) {
		// TODO Auto-generated method stub
		userMapper.update(userVO);
		return new Msg<>();
	}

}

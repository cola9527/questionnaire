package com.fusion.less.service.pro.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fusion.less.entity.pro.vo.UserAnswerVO;
import com.fusion.less.mapper.pro.UserAnswerMapper;
import com.fusion.less.service.pro.UserAnswerService;
import com.fusion.less.utils.json.Msg;

/** 
* @author cola 
* @version 创建时间：2019年9月8日 下午5:20:19 
*   说明
*/
@Service
public class UserAnswerServiceImpl implements UserAnswerService {
	
	@Autowired
	private UserAnswerMapper userAnswerMapper;

	@Override
	public Msg<?> add(List<UserAnswerVO> userAnswerVOList) {
		// TODO Auto-generated method stub
		userAnswerMapper.add(userAnswerVOList);
		return new Msg<>();
	}
	
	
	
}

package com.fusion.less.service.pro;

import java.util.List;

import com.fusion.less.entity.pro.vo.UserAnswerVO;
import com.fusion.less.utils.json.Msg;

/** 
* @author cola 
* @version 创建时间：2019年9月8日 下午5:18:35 
*   说明
*/

public interface UserAnswerService {

	/** 
	 *提交答案
	 * @param UserAnswerVO userAnswerVO
	 * @return
	 */ 
	Msg<?> add(List<UserAnswerVO> userAnswerVOList);
	
}

package com.fusion.less.utils.pagehelper;

import java.util.List;

import com.github.pagehelper.PageSerializable;

public class Page<T> extends PageSerializable<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 public Page() {
	    }

	    /**
	     * 包装Page对象
	     *
	     * @param list
	     */
	    public Page(List<T> list) {
	       super(list);
	    }
}

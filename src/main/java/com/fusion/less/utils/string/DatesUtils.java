package com.fusion.less.utils.string;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DatesUtils {

	public static String getNextDay(String date) {
		Calendar cal = Calendar.getInstance();
		if(date == null || "".equals(date))
		{
			return "";
		}
		
		int year = Integer.parseInt(date.substring(0, 4));
		int month = Integer.parseInt(date.substring(5, 7));
		int day = Integer.parseInt(date.substring(8,10));
		//设置年份
		cal.set(Calendar.YEAR,year);
		//设置月份
		cal.set(Calendar.MONTH, month-1);
		cal.set(Calendar.DATE, day);
		cal.setTimeInMillis((cal.getTimeInMillis()/1000+86400)*1000);
        //格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(cal.getTime());
	}
	public static void main(String[] args) {
		 
		System.out.println(getNextDay("2019-09-30"));
	}
	
	
}

package com.fusion.less.utils.string;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.env.Environment;


public final class FileHelper
{

	/**
	 * 写入文件
	 * 
	 * @param file
	 *            文件
	 * @param content
	 *            内容
	 * @return
	 */
	public static boolean writeFile(File file, String content)
	{
		return writeFile(file.getPath(), content, false);
	}

	/**
	 * 写入文件
	 * 
	 * @param filePath
	 *            文件路径
	 * @param content
	 *            内容
	 * @return
	 */
	public static boolean writeFile(String filePath, String content)
	{
		return writeFile(filePath, content, false);
	}

	/**
	 * 写入文件
	 * 
	 * @param filePath
	 * @param content
	 * @param append
	 * @return
	 */
	public static boolean writeFile(String filePath, String content, boolean append)
	{
		try {
			File file = new File(filePath);
			FileOutputStream out = new FileOutputStream(file, append);
			OutputStreamWriter fwout = new OutputStreamWriter(out, "UTF-8");
			BufferedWriter bw = new BufferedWriter(fwout);
			FileLock fl = out.getChannel().tryLock();
			if (fl.isValid()) {
				bw.write(content);
				fl.release();
			}
			bw.flush();
			fwout.flush();
			out.flush();
			bw.close();
			fwout.close();
			out.close();
			bw = null;
			fwout = null;
			out = null;
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

 
	/**
	 * 分块写入文件
	 * 
	 * @param filePath
	 * @param content
	 * @param off
	 * @param len
	 * @return
	 */
	public static boolean writeFile(String filePath, String content, int off, int len)
	{
		File file = new File(filePath);
		if (file.exists()) {
			FileOutputStream outputStream = null;
			OutputStreamWriter outputWriter = null;
			BufferedWriter bufWriter = null;
			try {
				outputStream = new FileOutputStream(filePath);
				outputWriter = new OutputStreamWriter(outputStream, "UTF-8");
				bufWriter = new BufferedWriter(outputWriter);
				FileLock fl = outputStream.getChannel().tryLock();
				if (fl.isValid()) {
					bufWriter.write(content, off, len);
					fl.release();
				}
				return true;
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					bufWriter.close();
					outputWriter.close();
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return false;
	}

	/**
	 * 分块读取文件
	 * 
	 * @param file
	 * @param len
	 * @return
	 */
	public static String readFile(File file, int len)
	{
		FileInputStream fis = null;
		BufferedInputStream bis = null;
		try {
			fis = new FileInputStream(file);
			bis = new BufferedInputStream(fis);
			byte datas[] = new byte[len];
			bis.read(datas, 0, len);
			return new String(datas, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (bis != null) {
					bis.close();
				}
				if (fis != null) {
					fis.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * 读取路径读取
	 * 
	 * @param filePath
	 * @return
	 */
	public static String readFile(String filePath)
	{
		return readFile(new File(filePath));
	}

	/**
	 * 通过文件读取
	 * 
	 * @param file
	 * @return
	 */
	public static String readFile(File file)
	{
		StringBuffer content = new StringBuffer();
		if (file != null && file.exists()) {
			FileInputStream fis = null;
			InputStreamReader isr = null;
			BufferedReader br = null;
			try {
				fis = new FileInputStream(file);
				isr = new InputStreamReader(fis, "UTF-8");
				br = new BufferedReader(isr);
				String temp = null;
				while ((temp = br.readLine()) != null) {
					content.append(temp);
					content.append("\n");
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (br != null) {
						br.close();
					}
					if (isr != null) {
						isr.close();
					}
					if (fis != null) {
						fis.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return content.toString().trim();
	}

	/**
	 * 把文件内容转为list
	 * 
	 * @param file
	 * @return
	 */
	public static List<String> readFileToList(File file)
	{
		List<String> lines = new ArrayList<String>();
		if (file != null && file.exists()) {
			FileInputStream fis = null;
			InputStreamReader isr = null;
			BufferedReader br = null;
			try {
				fis = new FileInputStream(file);
				isr = new InputStreamReader(fis, "UTF-8");
				br = new BufferedReader(isr);
				String temp = null;
				while ((temp = br.readLine()) != null) {
					lines.add(temp);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (br != null) {
						br.close();
					}
					if (isr != null) {
						isr.close();
					}
					if (fis != null) {
						fis.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return lines;
	}

	/**
	 * 通过流的形式得到资源
	 * 
	 * @param resource
	 * @return
	 * @throws FileNotFoundException
	 */
	public static InputStream getResourceAsStream(String resource) throws FileNotFoundException
	{
		String stripped = resource.startsWith("/") ? resource.substring(1) : resource;
		InputStream stream = null;
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		if (classLoader != null) {
			stream = classLoader.getResourceAsStream(stripped);
		}
		if (stream == null) {
			stream = Environment.class.getResourceAsStream(resource);
		}
		if (stream == null) {
			stream = Environment.class.getClassLoader().getResourceAsStream(stripped);
		}
		if (stream == null) {
			throw new FileNotFoundException(resource + " not found");
		}
		return stream;
	}

	/**
	 * **************************************************************
	 * 
	 * @Title: copy
	 * @Description: 文件复制
	 * @param srcImgPath
	 * @param destPath
	 * @return void
	 * @author huadong huadong19890803@163.com
	 * @date 2012-3-4
	 * @version V1.0
	 */
	public static void copy(String srcImgPath, String destPath) throws IOException
	{
		File fileOld = new File(srcImgPath);
		File fileNew = new File(destPath);
		if (fileOld.exists()) {
			FileInputStream fis = new FileInputStream(fileOld);
			FileOutputStream fos = new FileOutputStream(fileNew);
			try {
				int read = 0;
				while ((read = fis.read()) != -1) {
					fos.write(read);
					fos.flush();
				}
			} finally {
				fos.close();
				fis.close();
			}
		}
	}

	/**
	 * 创建文件
	 * 
	 * @Description: TODO
	 * @author xie
	 * @date 2015年1月26日 下午5:05:59
	 * @param path
	 * @return
	 */
	public static boolean createDir(String path)
	{
		try {
			File dir = new File(path);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * copy
	 * 
	 * @Description: TODO
	 * @author xie
	 * @date 2016年1月20日 下午12:29:26
	 * @param filePath
	 * @param inputStream
	 * @return
	 */
	public static boolean writeFile(String filePath, InputStream inputStream)
	{
		try {
			FileOutputStream fos = new FileOutputStream(filePath);
			try {
				int read = 0;
				while ((read = inputStream.read()) != -1) {
					fos.write(read);
				}
				fos.flush();
			} finally {
				fos.close();
				inputStream.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		/*
		 * try { OutputStream outputStream = new FileOutputStream(filePath); int
		 * bytesWritten = 0; int byteCount = 0;
		 * 
		 * byte[] bytes = new byte[1024];
		 * 
		 * while ((byteCount = inputStream.read(bytes)) != -1) { bytesWritten +=
		 * byteCount; outputStream.write(bytes, bytesWritten, byteCount); }
		 * outputStream.flush(); inputStream.close(); outputStream.close();
		 * return true; } catch (IOException e) { e.printStackTrace(); }
		 */
		return true;
	}
	
	/**
	 * 下载文件
	 * @Title downLoadFile
	 * @Descrption:TODO
	 * @param:@param fileUrl
	 * @param:@param savePath
	 * @param:@param fileName
	 * @return:void
	 * @auhor jiahy
	 * @date 2017年6月1日上午9:31:07
	 * @throws
	 */
	public static void downLoadFile(String fileUrl,String savePath,String fileName){
		URL url = null;
        HttpURLConnection con = null;
        InputStream in = null;
        FileOutputStream out = null;
        try {
            url = new URL(fileUrl);
            //建立http连接，得到连接对象
            con = (HttpURLConnection) url.openConnection();
            //con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
            in = con.getInputStream();
            byte[] data = getByteData(in);//转化为byte数组

            File file = new File(savePath);
            if (!file.exists()) {
                file.mkdirs();
            }

            File res = new File(file + File.separator + fileName);
            out = new FileOutputStream(res);
            out.write(data);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != out)
                    out.close();
                if (null != in)
                    in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
	}
	
	/**
     * 从输入流中获取字节数组
     * 
     * @param in
     * @return
     * @throws IOException
     */
    private static byte[] getByteData(InputStream in) throws IOException {
        byte[] b = new byte[1024];
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        int len = 0;
        while ((len = in.read(b)) != -1) {
            bos.write(b, 0, len);
        }
        if(null!=bos){
            bos.close();
        }
        return bos.toByteArray();
    }
    
    /**
     * TODO 删除指定路径和指定文件
     * @author jiahy
     * @param filepath 要删除的路径
     * @param fileName 要删除的文件
     * @param isdeleteDir
     * @return
     * @date 2017年6月8日上午8:36:09
     */
    public static boolean deleteFile(String filepath,String fileName,boolean isdeleteDir){
    	 File file = new File(fileName);
         // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
         if (file.exists() && file.isFile()) {
             if (file.delete()) {
            	 File fileDir =new File(filepath);
            	 if(fileDir.listFiles().length==0&&isdeleteDir){
            		 fileDir.delete();
            	 }
                 return true;
             } else {
                 return false;
             }
         } else {
             return false;
         }
    };
    /*
	 * Java文件操作 获取文件扩展名
	 *
	 * Created on: 2011-8-2 Author: blueeagle
	 */
	public static String getExtensionName(String filename) {
		if ((filename != null) && (filename.length() > 0)) {
			int dot = filename.lastIndexOf('.');
			if ((dot > -1) && (dot < (filename.length() - 1))) {
				return filename.substring(dot + 1);
			}
		}
		return filename;
	}

	/*
	 * Java文件操作 获取不带扩展名的文件名
	 *
	 * Created on: 2011-8-2 Author: blueeagle
	 */
	public static String getFileNameNoEx(String filename) {
		if ((filename != null) && (filename.length() > 0)) {
			int dot = filename.lastIndexOf('.');
			if ((dot > -1) && (dot < (filename.length()))) {
				return filename.substring(0, dot);
			}
		}
		return filename;
	}
	
    public static void deleteDir(String path,boolean isDeleteSelf){
		File file = new File(path);
		
		if(file.exists()){//判断是否待删除目录是否存在
			String[] content = file.list();//取得当前目录下所有文件和文件夹
			
			if(content.length == 0) {
				file.delete();
			}
			
			for(String name : content){
				File temp = new File(path, name);
				if(temp.isDirectory()){//判断是否是目录
					deleteDir(temp.getAbsolutePath(),true);//递归调用，删除目录里的内容
					temp.delete();//删除空目录
				}else{
					if(!temp.delete()){//直接删除文件
					}
				}
			}
		} 
		if(file.isDirectory() && isDeleteSelf) {
			file.delete();
		}
	}
}

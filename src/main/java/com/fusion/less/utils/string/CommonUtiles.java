package com.fusion.less.utils.string;
 
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
 
public class CommonUtiles {

    /**
     * 根据时间返回得到每一天的数组
     * @author yanyf
     * @date 2019年7月16日 下午7:39:43
     * @param startTime
     * @param endTime
     * @return
     */
    public static ArrayList<String> getTimeList(String startTime,String endTime) {
    	
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		long start;
		if(startTime != null && endTime != null) {
			try {
				start = df.parse(startTime).getTime();
				long end = df.parse(endTime).getTime();
				ArrayList<String> timeList=new ArrayList<String>();
				for (long i = start; i < end + 86400000; i = i + 86400000) {
					Date date = new Date();
					date.setTime(i);
					String da = df.format(date);
					timeList.add(da);
				}	
				
				return timeList;
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return null;

    	
    }
 
   
}


package com.fusion.less.mapper.base;

import java.util.ArrayList;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.fusion.less.entity.base.vo.TeamVO;

public interface TeamMapper {
	
	/**
	 * 查询全部组别
	 * @param 
	 * @return
	 */
	@Select("select * from t_bi_team")
	ArrayList<TeamVO> list();
	
	/**
	 * 查询组别
	 * @param id
	 * @return
	 */
	@Select("SELECT * FROM t_bi_team WHERE ID = #{id}")
	TeamVO get(int id);
	
	/**
	 * 更新组别
	 * @param team
	 */
	@Update({
		"<script>",
		"update t_bi_team set id = id ",
		" <when test='name != null '> ,name = #{name}</when> ",
		" where id = #{id}",
		"</script>"
	})
	void update(TeamVO teamVO);
	
	/**
	 * 添加组别
	 * 
	 * @param team
	 */
	@Insert("INSERT INTO t_bi_team (name, create_time) VALUES (#{name}, UNIX_TIMESTAMP(NOW()))")
	void add(TeamVO teamVO);
	
	/**
	 * 删除组别
	 * 
	 * @param id
	 */
	@Delete("DELETE FROM t_bi_team where ID = #{id}")
	void delete(int id);

}

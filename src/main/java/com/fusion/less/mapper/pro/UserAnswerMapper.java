package com.fusion.less.mapper.pro;

import java.util.List;

import org.apache.ibatis.annotations.Insert;

import com.fusion.less.entity.pro.vo.UserAnswerVO;

/** 
* @author cola 
* @version 创建时间：2019年9月8日 下午5:23:23 
*   说明
*/

public interface UserAnswerMapper {
	
	@Insert({"<script>",
		"insert into t_ques_user_answer(user_id, answer, create_time) values ",
		 "<foreach collection ='list' item='item' separator =','>",
	        "(#{item.userId}, #{item.answer}, UNIX_TIMESTAMP(NOW()))",
	      "</foreach>",
	      "</script>"})
	void add(List<UserAnswerVO> list);
}

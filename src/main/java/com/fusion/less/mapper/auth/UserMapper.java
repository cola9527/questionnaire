package com.fusion.less.mapper.auth;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.fusion.less.entity.auth.vo.UserVO;

/** 
* @author cola 
* @version 创建时间：2019年9月8日 下午2:22:21 
*   说明
*/

public interface UserMapper {
	
	/**
	 * 
	 * @param id
	 * @return UserVO userVO
	 */
	@Select("select * from t_auth_user where id = #{id}")
	UserVO get(int id);
	
	@Update({
		"<script>",
		"update t_auth_user set id = id ",
		" <when test='name != null '> ,name = #{name}</when> ",
		" where ID = #{id}",
		"</script>"
	})
	void update(UserVO userVO);
}

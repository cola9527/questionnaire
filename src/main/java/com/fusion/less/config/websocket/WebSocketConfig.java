//package com.fusion.sies.config.websocket;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.messaging.simp.config.MessageBrokerRegistry;
//import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
//import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
//import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
//
///**
// * 
// * @author louis
// *
// */
//@Configuration
//@EnableWebSocketMessageBroker
////通过EnableWebSocketMessageBroker 开启使用STOMP协议来传输基于代理(message broker)的消息,此时浏览器支持使用@MessageMapping 就像支持@RequestMapping一样。
//public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer{
//
//    @Override
//    public void registerStompEndpoints(StompEndpointRegistry registry) { //endPoint 注册协议节点,并映射指定的URl
//
//        //注册一个Stomp 协议的endpoint,并指定 SockJS协议
//        registry.addEndpoint("/wsWisely").setAllowedOrigins("*").withSockJS();
//
//        //注册一个名字为"endpointChat" 的endpoint,并指定 SockJS协议。   点对点-用
//        registry.addEndpoint("/wsChat").setAllowedOrigins("*").withSockJS();
//    }
//
//    @Override
//    public void configureMessageBroker(MessageBrokerRegistry registry) {//配置消息代理(message broker)
//        //广播式应配置一个/topic 消息代理
//        registry.enableSimpleBroker("/topic");
//
//        //点对点式增加一个/queue 消息代理
//        registry.enableSimpleBroker("/queue","/topic");
//    }
//    
//    @Bean
//    public SocketSessionRegistry SocketSessionRegistry(){
//        return new SocketSessionRegistry();
//    }
//    
//    @Bean
//    public STOMPConnectEventListener STOMPConnectEventListener(){
//        return new STOMPConnectEventListener();
//    }
//    
//   /* @Override
//    public void configureWebSocketTransport(final WebSocketTransportRegistration registration) {
//      registration.addDecoratorFactory(new WebSocketHandlerDecoratorFactory() {
//        @Override
//        public WebSocketHandler decorate(final WebSocketHandler handler) {
//          return new WebSocketHandlerDecorator(handler) {
//            @Override
//            public void afterConnectionEstablished(final WebSocketSession session) throws Exception {
//              // 客户端与服务器端建立连接后，此处记录谁上线了
//              String username = session.getPrincipal().getName();
//              System.out.println("online: " + username);
//              super.afterConnectionEstablished(session);
//            }
//
//            @Override
//            public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
//              // 客户端与服务器端断开连接后，此处记录谁下线了
//              String username = session.getPrincipal().getName();
//              System.out.println("offline: " + username);
//              super.afterConnectionClosed(session, closeStatus);
//            }
//          };
//        }
//      });
//      super.configureWebSocketTransport(registration);
//    }*/
//}
//

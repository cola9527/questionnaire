//package com.fusion.sies.config.websocket;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ApplicationListener;
//import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
//import org.springframework.web.socket.messaging.SessionConnectEvent;
//
//import lombok.extern.java.Log;
//
//@Log
//public class STOMPConnectEventListener implements ApplicationListener<SessionConnectEvent> {
//	
//	@Autowired
//    SocketSessionRegistry webAgentSessionRegistry;
//
//    @Override
//    public void onApplicationEvent(SessionConnectEvent event) {
//        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
//        //login get from browser
//        String agentId = sha.getNativeHeader("login").get(0);
//        String sessionId = sha.getSessionId();
//        log.info("用户注册成功："+sessionId+":"+agentId);
//        webAgentSessionRegistry.registerSessionId(agentId,sessionId);
//    }
//    
//}

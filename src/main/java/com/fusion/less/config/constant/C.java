package com.fusion.less.config.constant;

public interface C {
	
	//用户
	interface USER {
		//用户状态
		interface STATUS {
			int NORMAL = 0;//正常
			int FORBIDDEN = 1;//禁止的
			int OBSOLETE = 2;//作废的
		}
		
		interface TYPE {
			int PARENT = 0;//家长
			int TEACHER = 1;//教师
			int LEADER = 2;//领导
			int PAD = 3;//pad
			int ADMIN = 3;//管理员
		}
	}
	
	interface STATUS {
		String NORMAL = "0";//正常
		String FORBIDDEN = "1";//禁止的
		String OBSOLETE = "2";//作废的
	}

}

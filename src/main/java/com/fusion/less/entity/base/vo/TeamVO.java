package com.fusion.less.entity.base.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author cola
 *
 */

//@ApiModel(value="组别对象",description="组别基础实体")
@Data
public class TeamVO implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "id")
	// 组别的id
	private Integer id;
	
	@ApiModelProperty(value = "组别的名字")
	private String name;
	
	@ApiModelProperty(value = "创建记录的时间")
	private Long createTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getCreateTime() {
		return createTime; 
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}
	
}

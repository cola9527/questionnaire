package com.fusion.less.entity.auth.vo;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户
 * @author cola
 *
 */
//@ApiModel(value="用户对象",description="用户对象实体")
@Data
public class UserVO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "id")
	// 用户id
	private Integer id;
	
	@ApiModelProperty(value = "昵称")
	private String name;
	
	@ApiModelProperty(value = "手机号")
	@NotNull
	private String phone;
	
	@ApiModelProperty(value = "微信code")
	private String openId;
	
	@ApiModelProperty(value = "父id")
	private Integer parentId;
	
	@ApiModelProperty(value = "组别id")
	private Integer teamId;
	
	@ApiModelProperty(value = "创建时间")
	private Long createTime;

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}
	
	
}

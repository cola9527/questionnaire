package com.fusion.less.entity.auth.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 用户
 * @author cola
 *
 */
//@ApiModel(value="用户详细信息对象",description="用户详细信息对象实体")
@Data
public class ProfileVO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "id")
	private Integer id;
	
	@ApiModelProperty(value = "用户id")
	private Integer userId;
	
	@ApiModelProperty(value = "用户的真实名字")
	private String realName;
	
	@ApiModelProperty(value = "用户的身份证号")
	private String idCard;

	@ApiModelProperty(value = "用户所在的省")
	private String province;
	
	@ApiModelProperty(value = "用户所在的市(县)")
	private String city;
	
	@ApiModelProperty(value = "用户所在的区")
	private String area;
	
	@ApiModelProperty(value = "用户的详细地址")
	private String address;
	
	@ApiModelProperty(value="创建时间")
	private Long creatTime;
}

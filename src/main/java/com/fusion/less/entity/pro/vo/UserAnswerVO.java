package com.fusion.less.entity.pro.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/** 
* @author cola 
* @version 创建时间：2019年9月8日 下午2:02:54 
*   说明
*/
@Data
public class UserAnswerVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "id")
	private Integer id;
	
	@ApiModelProperty(value = "用户id")
	private Integer userId;
	
	@ApiModelProperty(value = "所有答案")
	private String answer;
	
	@ApiModelProperty(value = "创建时间")
	private Long createTime;
}

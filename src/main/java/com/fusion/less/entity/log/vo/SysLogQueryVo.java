//package com.fusion.less.entity.log.vo;
//
//import com.fusion.less.entity.BaseDto;
//
//import io.swagger.annotations.ApiModelProperty;
//import lombok.Data;
//
//@Data
//public class SysLogQueryVo extends BaseDto{
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//
//	@ApiModelProperty(value="操作时间")
//	private String createDate;
//	
//	@ApiModelProperty(value="操作人")
//	private String userName;
//	
//	@ApiModelProperty(value="操作内容")
//	private String content;
//}

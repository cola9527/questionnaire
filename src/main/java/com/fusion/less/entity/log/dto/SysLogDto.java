package com.fusion.less.entity.log.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SysLogDto implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value="id")
	private Integer id;
	
	@ApiModelProperty(value="key")
	private Integer key;
	
	@ApiModelProperty(value="用户ID")
	private String userId;
	
	@ApiModelProperty(value="操作")
	private String operation;
	
	@ApiModelProperty(value="方法名")
	private String method;
	
	@ApiModelProperty(value="参数")
	private String params;
	
	@ApiModelProperty(value="IP")
	private String ip;
	
	@ApiModelProperty(value="时间")
	private String createDate;
	
	@ApiModelProperty(value="内容")
	private String content;
	
}
